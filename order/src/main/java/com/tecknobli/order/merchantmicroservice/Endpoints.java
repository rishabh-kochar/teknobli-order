package com.tecknobli.order.merchantmicroservice;

public class Endpoints {

    public static final String BASE_URL="http://localhost:8080/merchant";

    public static final String ADDORDER_URL = "/order/add";

    public static final String SINGLE_MERCHANT_URL = "/select/";

    public static final String VALIDATE_URL = "/order/validateOrder";
}
