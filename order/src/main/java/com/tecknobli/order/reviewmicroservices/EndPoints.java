package com.tecknobli.order.reviewmicroservices;

public class EndPoints {

    public static final String BASE_URL="http://localhost:8000";
    public static final String GET_USER_PRODUCT_RATING = "/product/getUserRating";
    public static final String GET_USER_MERCHANT_RATING = "/merchant/getUserRating";
}
